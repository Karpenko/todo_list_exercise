"""Main file of our application.

When ran with:
$ python run.py
it will reset the database and start the webserver.
"""
import os
from todo import app, db


if __name__ == "__main__":

    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
