.. include:: ../../README.rst

Other sections of the documentation
-----------------------------------
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
