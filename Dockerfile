# Select base image from https://hub.docker.com/
FROM python:3.7.3

# Copy requirements first to leverage Docker caching
COPY ./requirements.txt /todo/requirements.txt

# Set working directory for RUN, CMD, ENTRYPOINT, COPY, and ADD commands
WORKDIR /todo

# Install pip packages
RUN pip install -r requirements.txt

# Copy all the code inside the container
COPY ./run.py /todo
COPY ./setup.py /todo
ADD ./tests /todo/tests
ADD ./todo /todo/todo

# Run "python run.py command"
CMD [ "python", "run.py" ]
