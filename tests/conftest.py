import os
import pytest
from flask import Flask, g, session

from todo import db as _db
from todo import views, auth
from todo.api import get_user, create_user


@pytest.fixture
def test_app():
    """Set up test Flask application."""
    # Set up Flask app
    app = Flask(__name__, template_folder='../todo/templates/')
    # We will create database in memory
    # That way, we don't worry about after cleaning/removing it after running tests
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///:memory:"
    app.config["TESTING"] = True
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
    app.config["SECRET_KEY"] = os.environ.get('KEY')
    app.register_blueprint(views.bp)
    app.register_blueprint(auth.bp)


    _db.init_app(app)

    with app.app_context():
        # Create database tables
        _db.create_all()
        create_user(username='test', password='test')
        create_user(username='test_1', password='test_1')
        g.user = get_user(user_id=1)
        app.register_blueprint(auth.bp)
        yield app
        # Our database is stored in memory so the following line is not needed
        # I'm using it to show you how to clean up after your tests
        _db.drop_all()


@pytest.fixture
def client(test_app):
    return test_app.test_client()


class AuthActions(object):
    def __init__(self, client):
        self._client = client

    def login(self, username='test_1', password='test_1'):
        return self._client.post(
            '/auth/login',
            data={'username': username, 'password': password}
        )

    def logout(self):
        return self._client.get('/auth/logout')


@pytest.fixture
def auth_function(client):
    return AuthActions(client)
