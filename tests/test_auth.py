import pytest
from flask import g, session

from todo.api import get_user


def test_register(client, test_app):
    assert client.get('auth/register').status_code == 200
    response = client.post('auth/register',
                           data={'username': 'a', 'password':'a'})
    assert 'http://localhost/auth/login' == response.headers['Location']

    with test_app.app_context():
        user_1 = get_user(username='a')
        assert user_1.username == 'a'
        assert user_1.id in [1,2,3]


@pytest.mark.parametrize(('username', 'password', 'message'), (
    ('', '', b'Username is required.'),
    ('a', '', b'Password is required.'),
    ('test', 'test', b'already registered'),
))
def test_register_validate_input(client, username, password, message):
    response = client.post(
        '/auth/register',
        data={'username': username, 'password': password}
    )
    assert message in response.data


def test_login(client, auth_function):
    assert client.get('/auth/login').status_code == 200
    response = auth_function.login()
    assert response.headers['Location'] == 'http://localhost/'

    with client:
        client.get('/')
        assert session['user_id'] == 2
        assert g.user.username == 'test_1'


@pytest.mark.parametrize(('username', 'password', 'message'), (
    ('a', 'test', b'Incorrect username.'),
    ('test', 'a', b'Incorrect password.'),
))
def test_login_validate_input(auth_function, username, password, message):
    response = auth_function.login(username, password)
    assert message in response.data


def test_logout(client, auth_function):
    auth_function.login()

    with client:
        auth_function.logout()
        assert 'user_id' not in session
