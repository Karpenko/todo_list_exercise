"""Tests for todo project."""

import pytest

from flask import g, session

from todo.api import (get_tasks, create_task, delete_task, finish_task,
                      unfinish_task, get_categories, create_category, delete_category,
                      edit_task, get_user)
from todo.models import Task, Category


def test_index_ui(client, auth_function):
    """Test basic login-logout icons function."""
    response = client.get('/')
    assert response.status_code == 302
    assert response.headers["Location"] == "http://localhost/auth/login"

    auth_function.login()
    response = client.get('/')
    assert b"Log Out" in response.data
    assert b"My TODO List" in response.data
    assert b"Add task" in response.data
    assert b"Add category" in response.data


@pytest.mark.parametrize(('path', 'result'), (
    ('/new_task', b'New task'),
    ('/new_category', b'New category'),
))
def test_login_required(client, path, result, auth_function):
    """Test that one cannot access new task or new category unlogged in."""
    response = client.get(path)
    assert response.headers['Location'] == 'http://localhost/auth/login'

    auth_function.login()
    response = client.get(path)
    assert result in response.data


@pytest.mark.parametrize('path', (
    '/done/1',
    '/undo/1',
    '/delete/1',
))
def test_login_required_tasks(client, path, auth_function):
    """Test that one cannot operate tasks of the other user.

    The idea behind here is to create a task by one user, and then try
    to delete, undo, or mark as done by the other."""
    create_task("buy apples", 1)
    task = Task.query.get(1)
    assert task.user_id == 1
    response = client.get(path)
    assert response.headers['Location'] == 'http://localhost/auth/login'

    auth_function.login()
    with client:
        response = client.get(path)
        assert session['user_id'] == 2
        assert response.status_code == 405


@pytest.mark.parametrize('path', (
    '/done/1',
    '/undo/1',
    '/delete/1',
))
def test_login_required_tasks(client, path, auth_function):
    """Test that one can operate tasks of themselves.

    The idea behind here is to create a task by a user, and then try
    to delete, undo, or mark as done by the same user."""
    g.user = get_user(user_id=2)
    create_task("buy cherries", 2)
    task = Task.query.get(1)
    assert task.user_id == 2
    response = client.get(path)
    assert response.headers['Location'] == 'http://localhost/auth/login'

    auth_function.login()
    with client:
        response = client.get(path)
        assert session['user_id'] == 2
        assert response.status_code == 302


@pytest.mark.parametrize('path', (
    '/task',
    '/category',
))
def test_task_and_category_create_ui(client, auth_function, path):
    """Login and create new category, and then new task."""
    response = client.post(path)
    assert response.headers['Location'] == 'http://localhost/auth/login'
    auth_function.login()
    with client:
        if path == '/category':
            response = client.post(path, data=dict(body='shopping'))
            assert response.status_code == 200
            category = Category.query.get(1)
            assert category.body == 'shopping'
        else:
            response = client.post(path, data=dict(body='buy bananas',
                                                   category=1))
            assert response.status_code == 302
            task = Task.query.get(1)
            assert task.body == 'buy bananas'


def test_task_list(test_app):
    """Test that get_tasks method works."""
    # Make sure no tasks exist
    assert get_tasks() == []
    # Create some tasks
    create_task("buy milk", 1)
    create_task("buy cookies", 1)
    # Make sure we have 2 tasks now
    assert len(get_tasks()) == 2


def test_create_tasks(test_app):
    """Test that create_task method works."""
    previous_length = len(get_tasks())
    create_task("brush teeth", 1)
    assert len(get_tasks()) > previous_length


def test_delete_task(test_app):
    """Test that delete_task method works."""
    previous_length = len(get_tasks())
    if not previous_length:
        create_task("Breakfast with pleasure", 1)
        previous_length = len(get_tasks())
    delete_task(task_id=1)
    assert len(get_tasks()) < previous_length


def test_finish_task(test_app):
    """Test that finish_task method works."""
    create_task("Play pm game", 1)
    task = Task.query.get(1)
    assert not task.done
    finish_task(task.id)
    assert task.done


def test_edit_task(test_app):
    """Test that task body gets changed."""
    create_task("Play chess", 1)
    task = Task.query.get(1)
    edit_task(1, "Play football")
    task = Task.query.get(1)
    assert task.body == "Play football"


def test_undo_task(test_app):
    """Test that undo method works."""
    create_task("Write tests", 1)
    task = Task.query.get(1)
    assert not task.done
    finish_task(task.id)
    assert task.done
    unfinish_task(task.id)
    assert not task.done


def test_category_id(test_app):
    """Test that category is saved upon task creation."""
    create_task("Test categories", 3)
    task = Task.query.get(1)
    assert task.category_id == 3


def test_create_category(test_app):
    """Test that category gets created."""
    categories = get_categories()
    assert len(categories) == 0
    create_category("Test category")
    assert len(get_categories()) == 1

def test_delete_category(test_app):
    """Test that category gets deleted."""
    create_category("Test category")
    assert len(get_categories()) == 1
    delete_category(1)
    assert len(get_categories()) == 0
