from setuptools import find_packages, setup


with open("README.rst", "r") as readme_file:
    readme = readme_file.read()


setup(
    name='todo_exercise',
    version='1.0.4',
    packages=find_packages(),
    include_package_data=True,
    description="todo_list flask app",
    long_description=readme,
    zip_safe=False,
    install_requires=[
        'flask','flask_sqlalchemy', 'flask_migrate',
    ],
)

