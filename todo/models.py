"""Database models for our application."""

from todo import db


class Category(db.Model):
    """Database table to store Todo tasks."""

    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text)
    tasks = db.relationship("Task", backref="category", lazy="dynamic")
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))


class Task(db.Model):
    """Database table to store Todo tasks."""

    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text)
    done = db.Column(db.Boolean, default=False)
    category_id = db.Column(db.Integer, db.ForeignKey("category.id"))
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    def __repr__(self):
        """Better string representation of our task."""
        return f"<Task(id={self.id}, body={self.body})>"


class User(db.Model):
    """Database table to store User info (used by auth)."""

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Text)
    password = db.Column(db.Text)

    def __repr__(self):
        """Representation of User by  username."""
        return f"<User(username={self.username})>"
