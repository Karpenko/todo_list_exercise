"""API functions to interact with TODO tasks."""

from flask import g

from werkzeug.security import generate_password_hash

from todo.models import Task, Category, User, db


def get_tasks():
    """Query all tasks filtered by user."""
    return Task.query.filter(Task.user_id == g.user.id).order_by(Task.category_id).all()


def get_categories():
    """Query all categories created by user."""
    return Category.query.filter(Category.user_id == g.user.id).all()


def get_user(username=None, user_id=None):
    """Select user from db by Username."""
    if username:
        return User.query.filter(User.username == username).first()
    elif user_id:
        return User.query.get(user_id)


def create_task(body, category):
    """Create new task object with body and commit to db."""
    task = Task(body=body, category_id=category, user_id=g.user.id)
    db.session.add(task)
    db.session.commit()


def create_category(body):
    """Create new category object with body and commit to db."""
    category = Category(body=body, user_id=g.user.id)
    db.session.add(category)
    db.session.commit()


def create_user(username, password):
    """Create new user."""
    user = User(username=username, password=generate_password_hash(password))
    db.session.add(user)
    db.session.commit()


def edit_task(task_id, body):
    """Edit body of the selected task."""
    task = Task.query.get(task_id)
    _check_user(task)
    task.body = body
    db.session.commit()


def delete_task(task_id):
    """Get object by task_id, delete, and commit to db."""
    task = Task.query.get(task_id)
    _check_user(task)
    db.session.delete(task)
    db.session.commit()


def delete_category(category_id):
    """Get object by category id, delete, and commit to db."""
    category = Category.query.get(category_id)
    _check_user(category)
    db.session.delete(category)
    db.session.commit()


def finish_task(task_id):
    """Get object by task_id, change .done to True, commit to db."""
    task = Task.query.get(task_id)
    _check_user(task)
    task.done = True
    db.session.commit()


def unfinish_task(task_id):
    """Get object by task_id, change .done to False, commit to db."""
    task = Task.query.get(task_id)
    _check_user(task)
    task.done = False
    db.session.commit()


def _check_user(task):
    if g.user.id != task.user_id:
        return
