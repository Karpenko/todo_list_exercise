"""URLs for our application."""

from flask import Blueprint, redirect, render_template, request
from todo.api import (
    get_tasks,
    get_categories,
    create_task,
    edit_task,
    delete_task,
    finish_task,
    unfinish_task,
    create_category,
    delete_category,
)

from todo.auth import login_required

bp = Blueprint("tasks", __name__, url_prefix="/")


@bp.route("/")
@login_required
def tasks_list():
    """Render the HTML at 'templates/application.html'."""
    tasks = get_tasks()
    categories = get_categories()
    # Render the HTML page located in "templates/application.html"
    # Passing tasks as a variable, so it can be used in the template
    return render_template("application.html", tasks=tasks, categories=categories)


@bp.route("/new_task")
@login_required
def task_form():
    """Render the HTML at 'templates/new_task.html'."""
    categories = get_categories()
    return render_template("new_task.html", categories=categories)


@bp.route("/new_category")
@login_required
def category_form():
    """Render the HTML at 'templates/new_category.html'."""
    return render_template("new_category.html")


@bp.route("/task", methods=["POST"])
@login_required
def task_create():
    """Get body & category from request and run 'api/create_task'."""
    body = request.form["body"]
    category = request.form["category"]
    create_task(body, category)
    # Redirect user to the main page, so the new task will be displayed
    return redirect("/")

@bp.route("/category")
@login_required
def category_list():
    """Render the HTML at 'templates/application.html'."""
    categories = get_categories()
    # Render the HTML page located in "templates/list_categories"
    return render_template("list_categories.html", categories=categories)



@bp.route("/category", methods=["POST"])
@login_required
def category_create():
    """Get body from request and run 'api/create_category'."""
    body = request.form["body"]
    create_category(body)
    categories = get_categories()
    # Redirect user to the main page, so the new task will be displayed
    return render_template("list_categories.html", categories=categories)


@bp.route("category/delete/<int:category_id>")
@login_required
def category_delete(category_id):
    """Run 'api/delete_category'."""
    delete_category(category_id)
    # Redirect user back to the main page, so the list of categorys will be updated
    return redirect("/category")


@bp.route("/edit", methods=["POST"])
@login_required
def task_edit():
    """Run api/edit_task."""
    result = request.get_json()
    body = result["body"]
    id = result["id"]
    edit_task(id, body)
    return redirect("/")


@bp.route("/delete/<int:task_id>")
@login_required
def task_delete(task_id):
    """Run 'api/delete_task'."""
    delete_task(task_id)
    # Redirect user back to the main page, so the list of tasks will be updated
    return redirect("/")


@bp.route("/done/<int:task_id>")
@login_required
def task_done(task_id):
    """Run 'api/finish_task'."""
    finish_task(task_id)
    return redirect("/")


@bp.route("/undo/<int:task_id>")
@login_required
def task_undone(task_id):
    """Run 'api/unfinish_task'."""
    unfinish_task(task_id)
    return redirect("/")
