"""By creating this file, we turn "todo" directory into a Python package.

Usually the __init__.py file is empty.
One of the reasons why you might put some code here is convenience.
All the functions and variables from this file can be imported with just:
from todo import app, db
"""
import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

DB_USER = os.environ.get("DB_USER")
DB_P = os.environ.get("DB_P")


# Set up Flask and sqlite database
app = Flask(__name__)
app.secret_key = os.environ.get("KEY")
# Comment out mysql settings and uncomment the following line to use temp db
#app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///todo.db"
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = f"mysql+pymysql://{DB_USER}:{DB_P}@localhost/todo"
#app.config[
#    "SQLALCHEMY_DATABASE_URI"
#] = f"mysql://bf72d761ba6b90:25e7a7b0@us-cdbr-east-04.cleardb.com/heroku_5b60fe94aba4833"
app.config["SESSION_TYPE"] = "filesystem"
# The following line is needed to suppress SQLALCHEMY_TRACK_MODIFICATIONS warning
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# This import has to be AFTER the "app" and "db" setup!
# Otherwise you will run into circular imports issues
# Here is another idea how to deal with them:
# https://stackoverflow.com/a/42910185
from todo import views, auth  # noqa E402 isort:skip

app.register_blueprint(views.bp)
app.register_blueprint(auth.bp)
