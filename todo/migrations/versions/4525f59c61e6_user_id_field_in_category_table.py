"""user_id field in category table

Revision ID: 4525f59c61e6
Revises: 8bc575c59421
Create Date: 2020-12-03 14:23:16.760857

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "4525f59c61e6"
down_revision = "8bc575c59421"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("category", sa.Column("user_id", sa.Integer(), nullable=True))
    op.create_foreign_key(None, "category", "user", ["user_id"], ["id"])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, "category", type_="foreignkey")
    op.drop_column("category", "user_id")
    # ### end Alembic commands ###
